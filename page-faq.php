<?php
/*
Template Name: FAQ Template
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article class="content container">
  <?php if (is_woocommerce_activated()) {
      $breadcrumb_args = array(
        'wrap_before' => '<div class="crumbs">',
        'wrap_after' => '</div>',
        'delimiter' => ' / '
      );
      woocommerce_breadcrumb( $breadcrumb_args );
    } else {
      qt_custom_breadcrumbs();
  } ?>
  <header>
    <h1 class="page-title"><?php the_title(); ?></h1>
  </header>

	<?php the_content(); ?>

  <?php endwhile; endif; ?>

  <?php $args = array(
  'post_type' => 'FAQ',
  'posts_per_page' => -1,
  'order_by' => 'menu-order',
  'order' => 'ASC' );
  $loop = new WP_Query( $args );
  while ( $loop->have_posts() ) : $loop->the_post();

    echo '<div class="question__wrapper" data-open="false">
            <h3 class="question__title">' . get_the_title() . '</h2>
            <div class="question__answer"><p class="question__answer-content">' . get_the_content() . '</p></div>
          </div>';

  endwhile; ?>
</article>

<?php get_footer(); ?>
