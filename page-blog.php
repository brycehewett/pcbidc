<?php
/*
Template Name: Blog Template
*/
?>
<?php get_header(); ?>

<div class="container content">
  <div class="two-third-column">
    <?php if (is_woocommerce_activated()) {
        $breadcrumb_args = array(
          'wrap_before' => '<div class="crumbs">',
          'wrap_after' => '</div>',
          'delimiter' => ' / '
        );
        woocommerce_breadcrumb( $breadcrumb_args );
      } else {
        qt_custom_breadcrumbs();
    } ?>

    <h1 class="page-title"><?php _e('Recent Articles','RAD'); ?></h1>

    <?php query_posts('posts_per_page='.get_option('posts_per_page').'&paged='. get_query_var('paged'));

      if( have_posts() ):
        while( have_posts() ): the_post();
        include('includes/post-teaser.php');
        endwhile;
        include('includes/post-nav.php');
      else: ?>

      <div class="post-404 noposts">
        <p><?php _e('No Posts Found','RAD'); ?></p>
      </div><!-- /.post-404 -->

    <?php endif; wp_reset_query(); ?>

  </div><!-- /.column-two-thirds -->

  <?php get_sidebar()?>

</div><!--/.tow-column-container-->

<?php get_footer(); ?>
