<?php
/*
Template Name: Team Template
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article class="container content">
  <?php if (is_woocommerce_activated()) {
      $breadcrumb_args = array(
        'wrap_before' => '<div class="crumbs">',
        'wrap_after' => '</div>',
        'delimiter' => ' / '
      );
      woocommerce_breadcrumb( $breadcrumb_args );
    } else {
      qt_custom_breadcrumbs();
  } ?>
  <header>
    <h1 class="page-title"><?php the_title(); ?></h1>
  </header>

	<?php the_content(); ?>
  <?php endwhile; endif; ?>

  <?php $args = array(
  'post_type' => 'team-members',
  'posts_per_page' => -1,
  'order_by' => 'menu-order',
  'order' => 'ASC' );
  $loop = new WP_Query( $args );

  while ( $loop->have_posts() ) : $loop->the_post();

    $twitter_url = get_post_meta($post->ID, 'twitter_url', true);
    $facebook_url = get_post_meta($post->ID, 'facebook_url', true);
    $instgram_url = get_post_meta($post->ID, 'instagram_url', true);
    $snapchat_url = get_post_meta($post->ID, 'snapchat_url', true);
    $linkedIn_url = get_post_meta($post->ID, 'linkedIn_url', true);
    $googlePlus_url = get_post_meta($post->ID, 'googlePlus_url', true);
    $pinterest_url = get_post_meta($post->ID, 'pinterest_url', true);

    echo '<div class="team-member">
      <div class="team-member__photo">' . ( has_post_thumbnail() ? get_the_post_thumbnail() : '') . '</div>
        <div class="team-member__info"><h3 class="team-member__name">' . get_the_title() . '</h3>' .
          ( $twitter_url ? '<a class="team-member__social-link twitter icon ion-social-twitter" href="' . $twitter_url . '" target="_blank"></a>' : '') .
          ( $facebook_url ? '<a class="team-member__social-link facebook icon ion-social-facebook" href="' . $facebook_url . '" target="_blank"></a>' : '') .
          ( $instgram_url ? '<a class="team-member__social-link instagram icon ion-social-instagram" href="' . $instgram_url . '" target="_blank"></a>' : '') .
          ( $snapchat_url ? '<a class="team-member__social-link snapchat icon ion-social-snapchat" href="' . $snapchat_url . '" target="_blank"></a>' : '') .
          ( $linkedIn_url ? '<a class="team-member__social-link linkedin icon ion-social-linkedin" href="' . $linkedIn_url . '" target="_blank"></a>' : '') .
          ( $googlePlus_url ? '<a class="team-member__social-link googleplus icon ion-social-googleplus" href="' . $googlePlus_url . '" target="_blank"></a>' : '') .
          ( $pinterest_url ? '<a class="team-member__social-link pinterest icon ion-social-pinterest" href="' . $pinterest_url . '" target="_blank"></a>' : '') .
          '<p class="team-member__bio">' . get_the_content() . '</p>' .
        '</div>' .
      '</div>';

  endwhile; ?>
</article>

<?php get_footer(); ?>
