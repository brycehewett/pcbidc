<div class="home__about">
  <div class="container">
    <div class="two-third-column">
      <div class="home__about__text-wrapper">
        <p>PCBIDC at Red Alert Diving is the Florida Panhandle’s newest PADI 5 Star Instructor Development Center. Located in beautiful Panama City Beach, PCBIDC is attached to a beautiful full service dive center and aquatic training facility.</p>
        <p>Red Alert Diving was created with the philosophy of “Only The Best in Training, Travel, Adventure and Equipment.” And having earned our 5 Star IDC rating at the end of 2019, we’re proud to bring that same mentality to professional development with PCBIDC.</p>
        <p>Our instructor trainers are committed to diving excellence and maintain the highest standards in the industry, with more than twenty years experience helping new dive professionals develop strong skills that carry them into fulfilling, long term careers in the water sports industry.</p>
        <p>Meanwhile, we’re constantly evolving, adding new curriculum and exciting opportunities for the seasoned dive pros who are ready to take their passion to the next level!</p>
        <p>Let us help you get started on the right foot. Begin by looking at our <a href="/course-schedule/">upcoming class schedule</a>, or give us a call right now and speak to one of our instructor trainers.</p>
      </div>
      <a class="btn__large btn__primary" href="tel:(850) 238-8760" target="_blank">Call Now</a>

      <div class="home__courses">
        <a class="home__courses__course home__courses__course__padi" href="/padi">PADI</a>
        <a class="home__courses__course home__courses__course__dan" href="/dan">DAN</a>
        <a class="home__courses__course home__courses__course__efr" href="/efr">EFR</a>
      </div>
    </div>
    <div class="card one-third-column last facebook-feed">
      <h3>Follow Us</h3>
      <div class="fb-page" data-href="https://www.facebook.com/RADSCUBA/" data-tabs="timeline" data-width="" data-height="895px" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/RADSCUBA/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/RADSCUBA/">Red Alert Diving</a></blockquote></div>
    </div>

  </div>
</div>
