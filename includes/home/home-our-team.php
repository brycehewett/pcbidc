<div class="home__team">
  <div class="container text__center home__recreation">
    <a class="btn__large btn__primary" href="https://redalertdiving.com" target="_blank">Learn About Our Recreational Courses  <i class="icon ion-share"></i></a>
  </div>

  <div class="container">
    <h2 class="page-title text__center">Meet Our Professionals</h2>
    <?php $args = array(
    'post_type' => 'team-members',
    'posts_per_page' => -1,
    'order_by' => 'menu-order',
    'order' => 'ASC' );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();

      echo '<div class="home__team__member">
        <div class="home__team__member__photo">' . ( has_post_thumbnail() ? get_the_post_thumbnail() : '') . '</div>
        <div class="home__team__member__info">
          <h3 class="home__team__member__name text__center">' . get_the_title() . '</h3>
        </div>
      </div>';

    endwhile; ?>
  </div>
</div>
