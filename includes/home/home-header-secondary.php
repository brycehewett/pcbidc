<div class="home__secondary-header-content">
  <div class="secondary-header-content secondary-header-content--column-one">
    <div>
      <h3>Dive Charters</h3>
      <a href="/charters" class="btn__default btn__white btn__allcaps">View Our Boat Schedule <i class="icon ion-chevron-right"></i></a>
    </div>
  </div>
  <div class="secondary-header-content secondary-header-content--column-two">
    <div>
      <h3>Learn to Dive</h3>
      <a href="/learn-to-dive" class="btn__default btn__white btn__allcaps">View Our Class Schedule <i class="icon ion-chevron-right"></i></a>
    </div>
  </div>
</div>
