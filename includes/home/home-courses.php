<div class="home__courses">
  <h2 class="text__center">Our Courses</h2>
  <div class="container">


    <a class="home__courses__course home__courses__course__pfi" href="/pfi">PFI</a>
    <a class="home__courses__course home__courses__course__naui" href="/naui">NAUI</a>
    <a class="home__courses__course home__courses__course__pro" href="/pro-training"><span class="home__courses__course__pro__text">Professional Training <i class="icon ion-chevron-right"></i></span></a>
  </div>
</div>
