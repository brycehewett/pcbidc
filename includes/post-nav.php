<?php
 if (is_singular()) {
	 $previous = get_previous_post_link('%link', '<i class="icons ion-ios-arrow-back"></i> Previous Post');
	 $next = get_next_post_link('%link', 'Next Post <i class="icons ion-ios-arrow-forward"></i>');
 } else {
	 $next = get_previous_posts_link('Newer Entries <i class="icons ion-ios-arrow-forward"></i>');
	 $previous = get_next_posts_link('<i class="icons ion-ios-arrow-back"></i> Older Entries');
 }

echo '<div class="post-nav">
				<div class="next-posts">' . $next . '</div>
				<div class="prev-posts">' . $previous . '</div>
			</div>';

?>
