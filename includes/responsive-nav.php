<nav class="responsive-menu">
  <?php $defaults = array(
    'menu'            => 'mobile-nav',
    'theme_location'  => 'mobile-nav',
    'container'       => '',
    'echo'            => true,
    'fallback_cb'     => false,
    'items_wrap'      => '<ul>%3$s</ul>',
    'depth'           => 0,);
  wp_nav_menu( $defaults ); ?>
</nav>
