<header>
  <?php if (is_home()) {
    $header_angle_class = 'header__angle__home';
    $header_background_class = 'home__header-background';
  } else {
    $header_angle_class = 'page__angle__home';
    $header_background_class = 'page__header-background';
  }?>

  <div class="header <?php echo $header_background_class ?>">
    <div class="top-nav">
      <div class="container no-padding">
        <a class="logo" href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>
        <?php $defaults = array(
          'menu'            => 'primary-menu',
          'theme_location'  => 'primary-menu',
          'container'       => '',
          'echo'            => true,
          'fallback_cb'     => false,
          'items_wrap'      => '<ul class="primary-nav">%3$s</ul>',
          'depth'           => 3,);
        wp_nav_menu( $defaults );

        if (is_woocommerce_activated()) {?>
          <div class="mobile-cart-nav">
            <?php include('nav-cart.php'); ?>
          </div>
          <?php } ?>
        <div class="menu-toggle"><i class="icon ion-navicon-round"></i></div>
      </div>
    </div>

    <?php (is_home() ? include('home/home-header.php'):'')?>
    <?php if (!is_home()) {?>
      <div class="header__angle <?php echo $header_angle_class ?>"></div>
    <?php }?>
  </div>
</header>
