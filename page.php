<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article class="container content" id="post-<?php the_ID(); ?>">
  <?php if (is_woocommerce_activated()) {
      $breadcrumb_args = array(
        'wrap_before' => '<div class="crumbs">',
        'wrap_after' => '</div>',
        'delimiter' => ' / '
      );
      woocommerce_breadcrumb( $breadcrumb_args );
    } else {
      qt_custom_breadcrumbs();
  } ?>
  <header>
    <h1 class="page-title"><?php the_title(); ?></h1>
  </header>
  <?php the_content(); ?>
</article>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
