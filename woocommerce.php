<?php get_header(); ?>

  <div class="container content">
    <?php $breadcrumb_args = array(
        'wrap_before' => '<div class="crumbs">',
        'wrap_after' => '</div>',
        'delimiter' => ' / '
      );woocommerce_breadcrumb( $breadcrumb_args ); ?>
    <?php woocommerce_content(); ?>
  </div>
</div>

<?php get_footer(); ?>
