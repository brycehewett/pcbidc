//=require jquery-doubletaptogo/dist/jquery.dcd.doubletaptogo.min.js

(function($){
$(document).ready(function (){
  //keep dropdownmenu from navigating until a second click is made
  $('.primary-nav').doubleTapToGo();
  //navigation toggle
  $('.menu-toggle').click( function(){
    $('.menu-toggle, .responsive-menu').toggleClass('active');
    $('.site-wrapper').toggleClass('pushed');
    $('html, body').toggleClass('menu-open');
    $('.menu-toggle .icon').toggleClass('ion-close-round');
    $('.menu-toggle .icon').toggleClass('ion-navicon-round');
  });
  //faq question toggle
  $('.question__title').click(function() {
    var wrapper = $(this).parent();
    var answerDiv = $(this).next();
    var answerScrollHeight = answerDiv.prop('scrollHeight');

    var openSection = function() {
      wrapper.attr('data-open', 'true');
      wrapper.addClass('open');
      $(answerDiv).animate({height: answerScrollHeight + 20 + 'px'}, 100)
    }

    var closeSection = function() {
      wrapper.attr('data-open', 'false');
      $(answerDiv).animate({height: 0 + 'px'}, 100, function() {
        wrapper.removeClass('open');
      })
    }

    if (wrapper.attr('data-open') == 'true') {
      closeSection();
    } else {
      openSection();
    }
  })

  //Resize FAQ section on window resize
  $(window).resize(function() {
    $('.question__wrapper').each(function(i){
      if ($(this).attr('data-open') == 'true') {
        var answerContentHeight = $(this).find('.question__answer-content').height();
        $(this).find('.question__answer').height(answerContentHeight + 20 + 'px');
      }
    })
  });

});
})(jQuery);
