<?php get_header(); ?>

<div class="container content">
  <div class="two-third-column" id="main-column">

    <h1 class="page-title">Search Results</h1>

    <?php if (have_posts()) : while (have_posts()) : the_post();

			include('includes/post-teaser.php');

				endwhile;

			include (TEMPLATEPATH . '/includes/nav.php' );

				else : echo '<h2>Sorry, your search returned no results.</h2>';

			endif;?>
  </div>
  <?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
